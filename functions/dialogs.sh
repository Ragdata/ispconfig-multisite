#-------------------------------------------------------------------
# functions/dialogs.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Multisite
#
# File:         functions/dialogs.sh
# Author:       Ragdata
# Date:         19/01/2021 0549
# License:      GNU GPLv3
# Copyright:    2021 ~ Aequitas Veritas Pty Ltd ~ All Rights Reserved
#-------------------------------------------------------------------
OpeningDialog()
{
  whiptail --title "ISPConfig 3.2+ Multsite Installer" --yesno "On the dialog boxes that follow, clicking 'OK' will move you forward to the next question, and clicking 'CANCEL' will take you back to the last question.  Are you ready to begin?" 10 75
}