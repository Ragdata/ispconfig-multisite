#-------------------------------------------------------------------
# functions/check_system.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Multisite
#
# File:         functions/check_system.sh
# Author:       Ragdata
# Date:         19/01/2021 0546
# License:      GNU GPLv3
# Copyright:    2021 ~ Aequitas Veritas Pty Ltd ~ All Rights Reserved
#-------------------------------------------------------------------
CheckLinux()
{
  echo -n "Checking that this is a Linux system ... "
  if ! echo "$OSTYPE" | grep -iq "linux"; then
    echo -e "${red}ERROR: This script must be run on Linux${NC}" >&2
    exit 1
  fi
  echo -e "[${green}DONE${NC}]\n"
}

CheckSystem()
{

  echo -n "Starting system scan ... "
  # Extract System Information
  . /etc/os-release

  DISTRO=''

  if echo "$ID" | grep -iq "ubuntu"; then

    if echo "$VERSION_ID" | grep -iq "14.04"; then
      DISTRO=ubuntu-14.04

    elif echo "$VERSION_ID" | grep -iq "15.10"; then
      DISTRO=ubuntu-15.10

    elif echo "$VERSION_ID" | grep -iq "16.04"; then
      DISTRO=ubuntu-16.04

    elif echo "$VERSION_ID" | grep -iq "16.10"; then
      DISTRO=ubuntu-16.10

    elif echo "$VERSION_ID" | grep -iq "17.10"; then
      DISTRO=ubuntu-17.10

    elif echo "$VERSION_ID" | grep -iq "18.04"; then
      DISTRO=ubuntu-18.04

    elif echo "$VERSION_ID" | grep -iq "20.04"; then
      DISTRO=ubuntu-20.04
    fi

  elif echo "$ID" | grep -iq "debian"; then
    DISTRO=Debian

  elif echo "$ID" | grep -iq "raspbian"; then
    DISTRO=Raspbian

  elif echo "$ID" | grep -iq "centos"; then
    DISTRO=CentOS

  elif echo "$ID" | grep -iq "opensuse"; then
    DISTRO=OpenSUSE

  elif echo "$ID" | grep -iq "fedora"; then
    DISTRO=Fedora

  fi

  echo -e "[${green}DONE${NC}]\n"
}

CheckRoot()
{
  echo -n "Checking script has root privileges ... "
  if [[ $(id -u) -ne 0 ]]; then
    echo -e "${red}ERROR: This script must be run as root${NC}" >&2
    exit 1
  fi
  echo -e "[${green}DONE${NC}]\n"
}

CheckTotalMemory()
{
  TOTALMEM=$(awk '/^MemTotal:/ {print $2}' /proc/meminfo)
  TOTALSWAP=$(awk '/^SwapTotal:/ {print $2}' /proc/meminfo)

  RAM_MiB=$(("$TOTALMEM" / 1024))

  MB1=$(("$TOTALMEM" * 1024))
  MB2=$(("$MB1" / 1000))
  RAM_MB=$(("$MB2" / 1000))

  PRINT_RAM_MiB=$(printf "%'d" "$RAM_MiB")
  PRINT_RAM_MB=$(printf "%'d" "$RAM_MB")

  if [ "$TOTALMEM" -lt 524288 ]; then
    echo "This machine has $PRINT_RAM_MiB MiB ($PRINT_RAM_MB) RAM (memory)"
    echo -e "\n${red}ERROR: ISPConfig requires at least 512 MiB RAM, with 1GiB RAM (1024 MiB) recommended${NC}"
    exit 1
  fi
}

CheckConnectivity()
{
  echo -n "Checking internet connection ... "
  if ! ping -q -c 3 www.ispconfig.org > /dev/null 2>&1; then
    echo -e "${red}ERROR: Could not reach ispconfig.org - please check your internet connection and run the script again${NC}" >&2
    exit 1
  fi
  echo -e "[${green}DONE${NC}]\n"
}

GetGlobalVars()
{
  HOSTNAME=$(hostname)
  HOSTNAME_FQDN=$(hostname -f)
  DOMAIN=$(hostname -d)
  IP_ADDRESS=( $(hostname -I) )
  RE='^2([0-4][0-9]|5[0-5])|1?[0-9][0-9]{1,2}(\.(2([0-4][0-9]|5[0-5])|1?[0-9]{1,2})){3}$'
  IPv4_ADDRESS=( $(for i in ${IP_ADDRESS[*]}; do [[ "$i" =~ $RE ]] && echo "$i"; done) )
  RE='^[[:xdigit:]]{1,4}(:[[:xdigit:]]{0,4}){6}$'
  IPv6_ADDRESS=( $(for i in ${IP_ADDRESS[*]}; do [[ "$i" =~ $RE ]] && echo "$i"; done) )
}