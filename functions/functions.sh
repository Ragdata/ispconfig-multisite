#-------------------------------------------------------------------
# functions/functions.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Multisite
#
# File:         functions/functions.sh
# Author:       Ragdata
# Date:         19/01/2021 0549
# License:      GNU GPLv3
# Copyright:    2021 ~ Aequitas Veritas Pty Ltd ~ All Rights Reserved
#-------------------------------------------------------------------
DefineColors() {
  # BASH COLOURS
  red='\e[0;31m'
  green='\e[0;32m'
  yellow='\e[0;33m'
  bold='\e[1m'
  underlined='\e[4m'
  NC='\e[0m' # No Color
  COLUMNS=$(tput cols)
}

InstallWhiptail() {
  if ! command -v whiptail >/dev/null; then
    echo -n "Installing Whiptail ... "
    apt install -y whiptail
    echo -e "[${green}DONE${NC}]\n"
  fi
}