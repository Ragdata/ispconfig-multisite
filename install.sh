#!/usr/bin/env bash
#-------------------------------------------------------------------
# install.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Multisite
#
# File:         install.sh
# Author:       Ragdata
# Date:         19/01/2021 0543
# License:      GNU GPLv3
# Copyright:    2021 ~ Aequitas Veritas Pty Ltd ~ All Rights Reserved
#-------------------------------------------------------------------
APWD=$(pwd)

source "$APWD"/functions/check_system.sh
source "$APWD"/functions/dialogs.sh
source "$APWD"/functions/functions.sh

defineColors

CheckLinux
CheckSystem
CheckRoot

CheckTotalMemory
CheckConnectivity

GetGlobalVars

echo "$IP_ADDRESS"
echo "$IPv4_ADDRESS"
echo "$IPv6_ADDRESS"

InstallWhiptail

clear

OpeningDialog
