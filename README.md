# ISPConfig-Multisite
[![Build Status](https://gitlab.com/Ragdata/ispconfig-multisite/badges/master/pipeline.svg)](https://gitlab.com/Ragdata/ispconfig-multisite/tree/master)

A multisite installer for ISPConfig 3.2+

## Installation
````
cd /opt
git clone git@gitlab.com:Ragdata/ispconfig-multisite.git
cd ispconfig-multisite
bash install.sh
````
## Version Control
[Semantic Versioning 2.0](https://semver.org/)
****
### Current Version
#### v.0.0.1
### Version History
#### v.1.0.0:   MVP
#### v.0.0.1:   Initial Commit